module.exports = {
  extends: 'eslint:recommended',
  parser: 'babel-eslint',
  plugins: ['prettier'],
  env: {
    node: true,
  },
  rules: {
    'prettier/prettier': ['error', { trailingComma: false, singleQuote: true }],
  },
};
