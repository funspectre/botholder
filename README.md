# Botholder

Botholder is a placeholder [Telegram][] [bot] that sends a message written in [markdown] or plain text.

## Usage

``` shell
# First, clone the repo
git clone https://gitlab.com/funspectre/botholder.git

# Change directory
cd botholder

# Create a MESSAGE file
cat "Hello World!" > MESSAGE.md

# Commit changes
git add MESSAGE.md
git commit -m "Add placeholder message"

# Finally, deploy

# Using now (https://now.sh)
now -e TELEGRAM_BOT_TOKEN="PUT_YOUR_TELEGRAM_BOT_TOKEN_HERE"

# Using heroku (https://heroku.com)
heroku create
heroku config:set TELEGRAM_BOT_TOKEN="PUT_YOUR_TELEGRAM_BOT_TOKEN_HERE" WEBHOOK_URL=$(heroku info -s | grep web_url | cut -d= -f2)
git push heroku master
```

## Configuration

You can configure the bot using the following environment variables.

-   `TELEGRAM_BOT_TOKEN` - your Telegram bot's API token
-   `PORT` - the local port to listen on for webhook updates
-   `WEBHOOK_URL` - the webhook's publicly accessible url

## Message

The message is parsed from the first file named `MESSAGE` matching any of the following extensions.

| Format     | Extensions               |
|------------|--------------------------|
| Markdown   | md, mdown, markdown      |
| Plain text | txt, (without extension) |

### Markdown Syntax

The markdown syntax to be used is specified by the [Telegram bot API][markdown]. Here's an excerpt.

```` text
*bold text*
_italic text_
[text](http://www.example.com/)
`inline fixed-width code`
```text
pre-formatted fixed-width code block
```
````

  [Telegram]: https://telegram.org
  [bot]: https://core.telegram.org/bots
  [markdown]: https://core.telegram.org/bots/api#markdown-style
