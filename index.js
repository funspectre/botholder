const fs = require('fs');
const debug = require('debug')('botholder');
const TelegramBot = require('node-telegram-bot-api');

const { PORT, TELEGRAM_BOT_TOKEN, WEBHOOK_URL, NOW_URL } = process.env;

const botOptions = {
  webHook: {
    port: PORT
  }
};

fs.readdir('.', (dirErr, list) => {
  const msgPathExp = /^message(\.(txt|md|mdown|markdown))/i;
  const markdownExp = /\.(md|mdown|markdown)$/i;
  const msgPath = list.filter(path => msgPathExp.test(path)).sort()[0] ||
    'MESSAGE.md.example';
  const msgParams = msgPath === 'MESSAGE.md.example' ||
    markdownExp.test(msgPath)
    ? {
        parse_mode: 'Markdown'
      }
    : {};
  fs.readFile(msgPath, (fileErr, msgText) => {
    if (fileErr) return debug(fileErr);

    const bot = new TelegramBot(TELEGRAM_BOT_TOKEN, botOptions);

    bot.setWebHook(`${WEBHOOK_URL || NOW_URL}/bot${TELEGRAM_BOT_TOKEN}`);

    return bot.on('message', ({ chat }) => {
      bot.sendMessage(chat.id, msgText, msgParams);
    });
  });
});
